// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require spree/frontend
//= require_tree .
//= require spree/frontend/add_to_cart_analytics
//= require spree/frontend/remove_from_cart_analytics


window.addEventListener('load', (event) =>{
    if (document.cookie.indexOf("ModalShown=true")<0) {
        $('body').addClass('mbody');
        $("#cookieModal").modal("show");

        $("#myModalClose").click(function () {
        	$("#cookieModal").modal("hide");
        });
        document.cookie = "ModalShown=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";


    }
});


window.fbAsyncInit = function() {
    FB.init({
        xfbml            : true,
        version          : 'v15.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


var chatbox = document.getElementById('fb-customer-chat');
chatbox.setAttribute("page_id", "726515504374048");
chatbox.setAttribute("attribution", "biz_inbox");
