Rails.application.routes.draw do
  default_url_options protocol: Rails.env.development? ? :http : :https
  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to
  # Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the
  # :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being
  # the default of "spree".
  mount Spree::Core::Engine, at: '/'

  post "/payments/buy_now_order", to: "spree/payments#buy_now_order"
  post "/payments/express_checkout", to: "spree/payments#express_checkout"

  #export data csv 
  get "/products_exports/pinterest", to: "spree/product_exports#pinterest"
  get "/products_exports/google", to: "spree/product_exports#google"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  get '404', to: 'application#page_not_found'
  get '422', to: 'application#server_error'
  get '500', to: 'application#server_error'


end
