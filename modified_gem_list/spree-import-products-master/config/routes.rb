Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :product_imports, only: [:index, :new, :create, :image_upload]
    get "/product_import_details", to: "product_imports#import_details"
    get "/aok", to: "product_imports#aok"
    get "/image_import", to: "product_imports#image_import"
  end
end