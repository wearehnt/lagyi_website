module Spree
  module Admin
    class ProductImportsController < BaseController


      def aok
                          
      end


      def image_import

        Dir.glob("lagyi_image/*") do |img|

          image = MiniMagick::Image.open("#{Rails.root}/lagyi_image/#{File.basename(img)}")

          ActiveStorage::Blob.create(
            key: SecureRandom::base58,
            filename: File.basename(img),
            content_type: File.extname(img).gsub(".","image/"),
            metadata:{"identified"=>true, "width"=> image[:width], "height"=>image[:height], "analyzed"=>true},
            service_name: "local",
            byte_size: File.size(img),
            checksum: Digest::SHA256.hexdigest(img),
            created_at: Time.now.strftime("%Y-%d-%m %H:%M:%S %Z")
          )
        end
      end

      def index
        redirect_to :action => :new
      end

      def new
        @product_import = Spree::ProductImport.new
      end

      def create
        @product_import = ProductImport.new(product_import_params)
        @product_import.file_name = product_import_params[:data_file].original_filename
        @product_import.user_id = spree_current_user.id
        @product_import.save!

        CreateProductsFromCsvJob.perform_later(@product_import)

        redirect_to admin_product_import_details_path
      end

      def import_details
        @product_imports = Spree::ProductImport.all.order("created_at DESC")
      end

      private
        def product_import_params
          params.require(:product_import).permit(:data_file)
        end
    end
  end
end
