# frozen_string_literal: true

module Imports
  class ImportProductsFromCsvUseCase < UseCase
    require 'csv'

    arguments :file, :product_import_id

    def persist
      import_csv
    end

    private

    def import_csv
      record_errors = []

      parsed_csv.each.with_index(1) do |row, index|
        next if product_exists?(row["name"])

        prototype_category = Spree::Prototype.find(row["prototype"])
        create_product(row, index,prototype_category, record_errors)
      end

      update_product_import(record_errors)
    end

    def update_product_import(record_errors)
      if record_errors.empty?
        product_import.update(status: "success")
      else
        product_import.update(status: "failed", import_errors: record_errors)
      end
    end

    def parsed_csv
      CSV.parse(file.download, headers: true, encoding: 'ISO8859-1', col_sep: ",", skip_blanks: true).delete_if do |row|
        row.to_hash.values.all?(&:blank?)
      end
    end

    def rand_time(from, to=Time.now)
      Time.at(rand_in_range(from.to_f, to.to_f))
    end

    def rand_in_range(from, to)
      rand * (to - from) + from
    end

    def create_product(row, index,prototype_category, record_errors)
      begin
        Spree::Product.transaction do

          # option values
          option_value_array = row["option_value"].gsub!(":",",")
          option_value = JSON.parse(option_value_array)
          option_value = Array(option_value.each_slice 2).to_h


          # size table
          if row["size"].nil?
            @size_table = "<table id='tblContent'>
                              <tr>
                                <td>Cell 1</td>
                                <td>Cell 2</td>
                                <td>Cell 3</td>
                              </tr>
                              <tr>
                                <td>Cell 4</td>
                                <td>Cell 5</td>
                                <td>Cell 6</td>
                              </tr>
                            </table>"
          else
            @sizes = row["size"].split(":").each_slice(3).to_a

            table1 = "<table id='tblContent'><tr><th>Size</th><th>Bust</th><th>Length</th></tr>"
            table2 = [] 
            @sizes.each do |title, bust, length| 
              table_row = "<tr><td>"+"#{title}"+"</td><td>"+"#{bust}"+"</td><td>"+"#{length}"+"</td></tr>" 
              table2 << table_row.to_s
            end
            table3 = "</table>"
            @size_table =  table1+table2.join(",")+table3
          end

          #product_name
          @product_name = row["name"].capitalize()

          if row["rating_content"].present?
            @reviews = 1
          else
            @reviews = 0
          end


          product = Spree::Product.create!(
            name: @product_name.encode('utf-8'),
            slug: @product_name.encode('utf-8'),
            sku: row["sku"].encode('utf-8'),
            shipping_category_id: 1,
            available_on: Date.today,
            price: row["price"],
            description: @size_table.gsub(',',''),
            prototype_id: prototype_category.id,
            option_values_hash: option_value,
            reviews_count: @reviews
          )
     
          query = "INSERT INTO spree_products_stores (product_id,store_id,created_at,updated_at) 
          VALUES('#{product.id}','#{Spree::Store.default.id}','#{Date.today}','#{Date.today}');"
          ActiveRecord::Base.connection.execute(query)

          taxon = Spree::Taxon.where(name: prototype_category.name).pluck(:id)

          query = "INSERT INTO spree_products_taxons (product_id,taxon_id,position,created_at,updated_at) 
          VALUES('#{product.id}','#{taxon.join.to_i}','1','#{Date.today}','#{Date.today}');"
          ActiveRecord::Base.connection.execute(query)
      
          if row["rating_content"].present?
            query = "INSERT INTO spree_reviews (product_id,name,location,rating,title,review,approved,created_at,updated_at,user_id,ip_address,locale,show_identifier) 
            VALUES('#{product.id}','Anonymous','#{row["rating_picture"]}','#{row["rating_number"]}','#{row["rating_content"]}','#{row["rating_content"]}','true','#{rand_time(20.days.ago)}','#{rand_time(2.days.ago)}','1','127.0.0.1','en','true');"
            ActiveRecord::Base.connection.execute(query)
          end

          variant_ids = Spree::Variant.where(product_id: product).pluck(:id)

          product_prices = Spree::Price.where(variant_id: variant_ids)
          product_prices.find_each do |price|
            Spree::Price.create(variant: price.variant, amount: price.amount * 0.85, currency: 'GBP')
          end

          Spree::StockItem.where(variant_id: variant_ids).update_all(count_on_hand: 99, backorderable: false)

          if row["material"].nil?
            material = "-" 
          else
            material = row["material"].encode('utf-8') 
          end

          if row["fit"].nil?
            fit = "-"
          else
            fit = row["fit"].encode('utf-8') 
          end

          if row["type"].nil?
            type = "-"
          else
            type = row["type"].encode('utf-8') 
          end
     
          #link product and property
          Spree::ProductProperty.where(position:1,product_id:product.id).update(value: material)
          Spree::ProductProperty.where(position:2,product_id:product.id).update(value: fit)
          Spree::ProductProperty.where(position:3,product_id:product.id).update(value: type)
  
            

        end

      rescue ActiveRecord::RecordInvalid => exception
        record_errors.push({ row_index: index, error_info: exception.record.errors.messages })
      end
    end

    def product_exists?(name)
      Spree::Product.where(name: name).exists?
    end

    def product_import
      @product_import ||= Spree::ProductImport.find(product_import_id)
    end
  end
end