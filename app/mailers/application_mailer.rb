class ApplicationMailer < ActionMailer::Base
  default from: 'customercare@loveubae.com'
  layout 'mailer'
end
