module Spree
  module ProductsFiltersHelper
      def price_filters
          @price_filters ||= [
            filters_less_than_price_range(30),
            filters_price_range(30, 35),
            filters_price_range(35, 45),
            filters_price_range(45, 50),
            filters_price_range(50, 65),
            filters_more_than_price_range(65)
          ]
        end
  end
end



