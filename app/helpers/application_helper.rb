module ApplicationHelper

 def get_shipping_cost(current_currency)
 	if current_currency == "GBP"
    	return 20.00
    else
        return 25.00
    end
 end

 def bulk_taxon(taxon_name)
   if taxon_name == "Bulk Orders"
      return "3 For"
   end
 end

end
