module Spree
  class PaymentsController < BaseController

	  def check_state_id(state)
			unless state.nil?
			   return state.id.to_i
			else
			   return nil
			end
		end


		def check_current_user
			 unless spree_current_user.nil? 
			 		return spree_current_user.id
			 else
			 		return nil
			 end
		end

		def create_address
			address = Spree::Address.create!(
	      		firstname: params[:firstname],
	      		lastname: params[:lastname],
	      		address1: params[:address1],
	      		zipcode: params[:zipcode],
	      		phone: params[:phone],
	      		city: params[:address2],
	      		country_id: @country.id.to_i,
	      		state_id: @state_id)

	        return address
		end

		def create_order
			order = Spree::Order.create!(
	      		email: params[:email],
	      		item_total: params[:item_total],
	      		total: params[:total_amount],
	      		state: "complete",
	      		bill_address_id: @address.id,
	      		ship_address_id: @address.id,
	      		payment_total: params[:total_amount],
	      		shipment_total: params[:shipment_total],
	      		item_count: params[:item_count],
	      		confirmation_delivered: true,
	      		completed_at: DateTime.now,
	      		currency: current_currency)

	    	Spree::LineItem.create!(
	     		variant_id: params[:variant_id],
	     		order_id: order.id,
	     		quantity: params[:item_count],
	     		price: params[:unit_amount],
	     		currency: current_currency)

			return order
		end

		def create_payment
		    Spree::Payment.create!(
		       amount: params[:total_amount].to_i,
		       order_id: @order.id,
		       # 
		       # payment_method_id: 1,
		       payment_method_id: 2,
		       source_id: 2,
		       source_type: Spree::PaymentMethod::Check,
		       state: "checkout")
		end

		def create_shipment
			shipment = Spree::Shipment.create!(
	       	   cost: params[:shipment_total],
	       	   order_id: @order.id,
	       	   address_id: @address.id,
	           state: "pending",
	           stock_location_id: 1)

			shipping_method = Spree::ShippingMethod.where(name: params[:shipment_name]).select(:id).first

			Spree::ShippingRate.create!(
	       		shipment_id: shipment.id,
	       		shipping_method_id: shipping_method.id.to_i,
	       		selected: true,
	       		cost: params[:shipment_total])

	
		end

		def update_shipment_state(order)
	    	if spree_current_user.nil? 
		      order.update(shipment_state: "pending",payment_state: "paid")
		    else
		      order.update(shipment_state: "pending",payment_state: "paid",user_id:spree_current_user.id)
		    end
		end


		def updat_order
				order = Spree::Order.where(id:params[:order_id].to_i)
				order.update_all(email: params[:email],
		              item_total: params[:item_total],
		              total: params[:total_amount],
		              state: "complete",
		              bill_address_id: @address.id,
		              ship_address_id: @address.id,
		              payment_total: params[:total_amount],
		              shipment_total: params[:shipment_total],
		              item_count: params[:item_count],
		              confirmation_delivered: true,
		              completed_at: DateTime.now,
		              shipment_state: "pending",
		              payment_state: "paid",
		              user_id: check_current_user,
		              currency: current_currency)
		end

		def get_country
			@country = Spree::Country.where(iso: params[:country]).select(:id).first
	    state = Spree::State.where(country_id: @country.id.to_i, abbr: params[:state]).select(:id).first
	    @state_id = check_state_id(state)
		end

	  def buy_now_order
	 		get_country
	    	@address = create_address
	    	@order = create_order
	    	
	    	create_payment
	    	create_shipment

	    	update_shipment_state(@order)
	    	render json: {order_number: @order.number}
	  end

	  def express_checkout
    		get_country
    		@address = create_address
    		@order = Spree::Order.find(params[:order_id].to_i)

    		create_payment
	    	create_shipment
	  		updat_order

	    	render json: {order_number: @order.number}
    	end

    end
end