module Spree
    class HomeController < Spree::StoreController
      respond_to :html

      def index
        @taxons = Spree::Taxon
        .includes(:products)
        .joins(:products)
      end

    end
end