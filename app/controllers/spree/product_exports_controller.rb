module Spree
  class ProductExportsController < BaseController
    require 'csv'
    respond_to :csv
    before_action :set_category
    helper_method :check_stock, :price_to_decimal, :fix_option_color, :fix_option_size

    def pinterest  
      @products = Spree::Product.all
      render template: "spree/product_exports/export_product_data_pinterest.csv.erb"
    end

    def google  
      @products = Spree::Product.left_outer_joins(variants_including_master: :option_values).where.not("spree_option_values.name IS 'as per photo'").distinct
      render template: "spree/product_exports/export_product_data_google.csv.erb"
    end

    def check_stock(variant)
      unless variant < 1
         return "in_stock"
       else
         return "out_of_stock"
      end
    end


    def fix_option_color(variant)
      if variant.option_values.last.name == "F"
        return variant.option_values.first.name
      else 
        return variant.option_values.last.name
      end
    end

    def fix_option_size(variant)
      if variant.option_values.last.name == "F"
        return 'one_size'
      else 
        return variant.option_values.first.name
      end
    end

    def price_to_decimal(price)
       product_price = "%.2f" % price
       return product_price.to_s + " USD"
    end

    def set_category
      @google_category = "Apparel & Accessories > Clothing > Shirts & Tops"
    end

  end
end