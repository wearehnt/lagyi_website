module Spree
  class TaxonsController < Spree::StoreController
    include Spree::FrontendHelper
    include Spree::CacheHelper
    include ApplicationHelper
    helper 'spree/products'

      def load_products
        search_params = params.merge(
          current_store: current_store,
          taxon: @taxon,
          include_images: true
        )

        @searcher = build_searcher(search_params)
        @products = @searcher.retrieve_products.order(created_at: :desc)
        @bulk_order = bulk_taxon(@taxon.name)
     
      end

    end
  end