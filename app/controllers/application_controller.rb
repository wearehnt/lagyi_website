class ApplicationController < ActionController::Base

	skip_before_action :verify_authenticity_token
	before_action :check_paypal_url, :prepare_meta_data

	def page_not_found
	    respond_to do |format|
	      format.html { render template: 'spree/errors/not_found', layout: 'layouts/application', status: 404 }
	      format.all  { render nothing: true, status: 404 }
	    end
	  end

	def server_error
	    respond_to do |format|
	      format.html { render template: 'spree/errors/forbidden', layout: 'layouts/application', status: 500 }
	      format.all  { render nothing: true, status: 500}
	    end
	end

	def check_paypal_url
		if request.original_url.include? "payment"
			@disable_payment_options = 'venmo,paylater'
		else
			@disable_payment_options = 'venmo,paylater,credit,card'
		end
		@client_id = check_paypal_client
	end

	def check_paypal_client
		if Rails.env.production?
  		return "AajQ0WZm5Tq9Ubqr3L40ku8Re43e7iQ1qnyHmzcVTqj24UWUWy8--5PbSjrd97Gw--2EPyP_TCUb1a5f"
  	else
  		return "test"
  	end
	end

  def prepare_meta_data
    @og_image_url = "https://www.mingalarburma.store/assets/bae_logo-3fd89cc4131a5977b1ab0e2decc35be76bad10f5b418f748ca2360fc3eec7bff.png"
    @meta_description = "Loveubae Clothing is an international B2C fast fashion brand that has been built on the idea of making clothing affordable."
  end
end
