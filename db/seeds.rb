# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Spree::Core::Engine.load_seed if defined?(Spree::Core)
Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

Spree::Prototype.create ([
	{ name: 'Sweat Collections',preferences: nil}, 
	{ name: 'Hoodies and Causual Jackets',preferences: nil},
	{ name: 'Shirts',preferences: nil},
	{ name: 'T-shirts',preferences: nil},
	{ name: 'Bulk Orders',preferences: nil},
	{ name: 'Fur Friends',preferences: nil},
	{ name: 'Wardrobe Essentials',preferences: nil}
])

Spree::Property.create ([
	{ name: 'material', presentation: 'Material', filterable: false}, 
	{ name: 'fit', presentation: 'Fit', filterable: false},
	{ name: 'type', presentation: 'Type', filterable: false}
])

Spree::PropertyPrototype.create ([
	{ prototype_id:1,property_id:1,preferences: nil}, 
	{ prototype_id:1,property_id:2,preferences: nil}, 
	{ prototype_id:1,property_id:3,preferences: nil}, 

	{ prototype_id:2,property_id:1,preferences: nil}, 
	{ prototype_id:2,property_id:2,preferences: nil},
	{ prototype_id:2,property_id:3,preferences: nil},  

	{ prototype_id:3,property_id:1,preferences: nil}, 
	{ prototype_id:3,property_id:2,preferences: nil}, 
	{ prototype_id:3,property_id:3,preferences: nil}, 

	{ prototype_id:4,property_id:1,preferences: nil}, 
	{ prototype_id:4,property_id:2,preferences: nil}, 
	{ prototype_id:4,property_id:3,preferences: nil},

	{ prototype_id:5,property_id:1,preferences: nil}, 
	{ prototype_id:5,property_id:2,preferences: nil}, 
	{ prototype_id:5,property_id:3,preferences: nil},

	{ prototype_id:6,property_id:1,preferences: nil}, 
	{ prototype_id:6,property_id:2,preferences: nil}, 
	{ prototype_id:6,property_id:3,preferences: nil},

	{ prototype_id:7,property_id:1,preferences: nil}, 
	{ prototype_id:7,property_id:2,preferences: nil}, 
	{ prototype_id:7,property_id:3,preferences: nil}

])

Spree::OptionType.create ([
	{ name: 'size', presentation: 'Size', filterable: true, preferences: nil},
	{ name: 'color', presentation: 'Color', filterable: true, preferences: nil} 
])


Spree::OptionTypePrototype.create ([
	{ prototype_id:1,option_type_id:1}, 
	{ prototype_id:1,option_type_id:2},

	{ prototype_id:2,option_type_id:1}, 
	{ prototype_id:2,option_type_id:2},

	{ prototype_id:3,option_type_id:1}, 
	{ prototype_id:3,option_type_id:2}, 

	{ prototype_id:4,option_type_id:1}, 
	{ prototype_id:4,option_type_id:2},

	{ prototype_id:5,option_type_id:1}, 
	{ prototype_id:5,option_type_id:2},

	{ prototype_id:6,option_type_id:1}, 
	{ prototype_id:6,option_type_id:2},

	{ prototype_id:7,option_type_id:1}, 
	{ prototype_id:7,option_type_id:2}


])

Spree::OptionValue.create ([
	#size
	{ position:1, name: "S", presentation:"S",option_type_id:1},
	{ position:2, name: "M", presentation:"M",option_type_id:1},
	{ position:3, name: "L", presentation:"L",option_type_id:1},
	{ position:4, name: "XL", presentation:"XL",option_type_id:1},
	{ position:5, name: "2XL", presentation:"2XL",option_type_id:1},
	{ position:6, name: "3XL", presentation:"3XL",option_type_id:1},
	{ position:7, name: "4XL", presentation:"4XL",option_type_id:1},
	#color
	{ position:1,name:"army green", presentation:"#4B5320",option_type_id:2},
	{ position:2,name:"black", presentation:"#000000",option_type_id:2},
	{ position:3,name:"dark blue", presentation:"#00008b",option_type_id:2},
	{ position:4,name:"dark brown", presentation:"#654321",option_type_id:2},
	{ position:5,name:"dark gray", presentation:"#666666",option_type_id:2},
	{ position:6,name:"dark green", presentation:"#008055",option_type_id:2},
	{ position:7,name:"dark red", presentation:"#660000",option_type_id:2},
	{ position:8,name:"ivory", presentation:"#faf0dd",option_type_id:2},
	{ position:9,name:"light brown", presentation:"#C19A6B",option_type_id:2},
	{ position:10,name:"light gray", presentation:"#cccccc",option_type_id:2},
	{ position:11,name:"light pink", presentation:"#FFC0CB",option_type_id:2},
	{ position:12,name:"light purple", presentation:"#ca80ff",option_type_id:2},
	{ position:13,name:"olive green", presentation:"#b3b300",option_type_id:2},
	{ position:14,name:"red", presentation:"#FF0000",option_type_id:2},
	{ position:15,name:"sky blue", presentation:"#87CEEB",option_type_id:2},
	{ position:16,name:"white", presentation:"#ffffff",option_type_id:2},
	{ position:17,name:"yellow", presentation:"#ffff00",option_type_id:2},
	{ position:18,name:"dark pink", presentation:"#df2059",option_type_id:2},
	{ position:19,name:"light green", presentation:"#99ff99",option_type_id:2},
	{ position:20,name:"purple", presentation:"#800080",option_type_id:2},
	{ position:21,name:"blue", presentation:"#3333ff",option_type_id:2},
	{ position:22,name:"orange", presentation:"#ff5c00",option_type_id:2},
	#added size
	{ position:8,name:"F", presentation:"F",option_type_id:1},		
	#add as per photo
	{ position:22,name:"as per photo", presentation:"#ff5c00",option_type_id:2},
	#added size
	{ position:9,name:"XS", presentation:"XS",option_type_id:1}																			

])

Spree::Taxonomy.create ([
  { name: "Shop All",position: 1,store_id: 1 },
  { name: "Limited Edition",position: 1,store_id: 1 },
  { name: "Bulk Orders",position: 1,store_id: 1 }
])

Spree::Taxon.create ([

  { position: 0, name: "Shop All",permalink: "shop_all", taxonomy_id: 1,hide_from_nav: false },
  { parent_id: 1,position: 0,name: "Sweat Collections",permalink: "shop_all/sweat_collection",taxonomy_id: 1,hide_from_nav: false, description:"These cozy sweaters will keep you warm all winter long. Our sweaters are made of high-quality materials, so it's sure to last. Plus, it's very affordable, so you can buy one for everyone in the family." },
  { parent_id: 1,position: 0,name: "Hoodies and Causual Jackets",permalink: "shop_all/hoodies-and-jackets",taxonomy_id: 1,hide_from_nav: false, description:"Looking for a stylish and unique hoodie that won't break the bank? Check out our selection of trend-setting hoodies for men and women. With prices starting at just $15, you're sure to find the perfect style to suit your taste and budget." },
  { parent_id: 1,position: 0,name: "Shirts",permalink: "shop_all/shirts",taxonomy_id: 1,hide_from_nav: false, description:"Looking for a new shirt that will make you look sharp and stylish? Check out our selection of shirts you're sure to find one that's perfect for you. With a variety of designs and colors to choose from, we've got just the shirt you're looking for." },
  { parent_id: 1,position: 0,name: "T-shirts",permalink: "shop_all/t-shirts",taxonomy_id: 1,hide_from_nav: false, description:"Looking for a stylish t-shirts to add to your wardrobe? Check out our collection of various design t-shirts you're sure to find one that's perfect for you!." },
  { parent_id: 1,position: 0,name: "Bulk Orders",permalink: "shop_all/bulk-orders",taxonomy_id: 1,hide_from_nav: false, description:"whole sale" },
	{ parent_id: 1,position: 0,name: "Fur Friends",permalink: "shop_all/fur-friends",taxonomy_id: 1,hide_from_nav: false, description:"Fur Friends" },
	{ parent_id: 1,position: 0,name: "Wardrobe Essentials",permalink: "shop_all/wardrobe-essentials",taxonomy_id: 1,hide_from_nav: false, description:"wardrobe" }

])

Spree::Menu.create ([
  { name: "Main Menu",location: "header",locale: "en",store_id: 1 }
 ])
  
Spree::MenuItem.create ([

  { name: "Autumn Streetwear",new_window: false,item_type: "Container",linked_resource_type: "URL",menu_id: 1},
  { name: "Categories",new_window: false,item_type: "Container",linked_resource_type: "URL",code: "category",parent_id: 2,menu_id: 1},

  { name: "Sweat Collections",new_window: false,item_type: "Link",linked_resource_type: "Spree::Taxon",linked_resource_id: 4,parent_id: 3,menu_id: 1},
  { name: "Hoodies and Causual Jackets",new_window: false,item_type: "Link",linked_resource_type: "Spree::Taxon",linked_resource_id: 5,parent_id: 3,menu_id: 1},
  { name: "Shirts",new_window: false,item_type: "Link",linked_resource_type: "Spree::Taxon",linked_resource_id: 6,parent_id: 3,menu_id: 1},
  { name: "T-shirts",new_window: false,item_type: "Link",linked_resource_type: "Spree::Taxon",linked_resource_id: 7,parent_id: 3,menu_id: 1},
 
  { name: "Bulk Orders",new_window: false,item_type: "Link",linked_resource_type: "Spree::Taxon",linked_resource_id: 8,menu_id: 1},
  { name: "Fur Friends",new_window: false,item_type: "Link",linked_resource_type: "Spree::Taxon",linked_resource_id: 9,menu_id: 1},

  { name: "Wardrobe Essentials",new_window: false,item_type: "Link",linked_resource_type: "Spree::Taxon",linked_resource_id: 10,parent_id: 3,menu_id: 1}

 
 ])

Spree::RelationType.create ([
	
  { name: "Related Products", description: "desc", applies_to: "Spree::Product" }
])


Spree::Store.where(id:1).update_all(name:"Lagyi BackendSystem")












